"""
https://habrahabr.ru/post/133780/
https://docs.python.org/2/reference/grammar.html
https://docs.python.org/2/reference/lexical_analysis.html#indentation
https://habrahabr.ru/search/?q=%5B%D0%BA%D0%BE%D0%BC%D0%BF%D0%B8%D0%BB%D1%8F%D1%86%D0%B8%D1%8F%5D&target_type=posts
http://kit.kulichki.net/crenshaw/tutor1.html
http://index-of.es/OS/Create%20Your%20Own%20Programming%20Language.pdf
https://habrahabr.ru/post/140058/
http://qaru.site/questions/6061/learning-to-write-a-compiler
https://github.com/aosabook/500lines/blob/master/interpreter/interpreter.markdown

program: (NEWLINE | statement)*
statement: simple_statement | complex_statement
simple_statement: (assignment | expression | return_statement) NEWLINE
return_statement: 'return' expression?
complex_statement: if_statement | while_statement | func_statement
if_statement: 'if' test ':' block ['else' ':' block]
while_statement: 'while' test ':' block
func_statement: 'def' NAME parameters ':' block
parameters: '(' [ NAME (, NAME)* ] ')'
block: NEWLINE INDENT statement+ DEDENT
test: expression comparision_operator expression
comparision_operator: '==' | '>=' | '<=' | '>' | '<'
assignment: atom '=' expression
expression: term (('+' | '-') term)*
term: factor (('*' | '/' | '%'| '//' ) factor)*
factor: atom trailer*
atom: ('[' [listmaker] ']' | NAME | NUMBER)
trailer: '(' [arglist] ')' | '[' expression ']'
listmaker: expression [, expression]*
"""
import operator
import re


class LexerError(Exception):
    pass


class Lexer:
    NUMBER_INT = 'INT'
    NUMBER_DOUBLE = 'DOUBLE'
    IDENT = 'IDENT'
    INDENT = 'INDENT'
    DEDENT = 'DEDENT'
    KEYWORDS = ('def', 'while', 'if', 'else', 'return')

    NUMBER_INT_RE = re.compile(r'[+-]?\d+')
    NUMBER_DOUBLE_RE = re.compile(r'[+-]?(\d+\.\d+|\d+\.|\.\d+)')
    KEYWORD_RE = re.compile(r'([_a-zA-Z]+\w*)')
    INDENT_START_RE = re.compile(r':\n( +)', re.M)
    INDENT_CONT_RE = re.compile(r'\n( *)', re.M)
    OPERATOR_RE = re.compile(r'==|>=|<=|>|<|=|\(|\)|\[|\]|,|\+|-|\*|//|/')

    def run(self, code):
        code = code.rstrip()
        code = code.lstrip('\n')
        if code.startswith(' '):
            raise LexerError('unexpected indent')
        tokens = []
        i = 0
        indents = [0]
        while i < len(code):
            chunk = code[i:]

            m = self.KEYWORD_RE.match(chunk)
            if m:
                ident = m.group()
                if ident in self.KEYWORDS:
                    tokens.append((ident.upper(), ident))
                else:
                    tokens.append((self.IDENT, ident))
                i += len(ident)
                continue

            m = self.NUMBER_DOUBLE_RE.match(chunk)
            if m:
                tokens.append((self.NUMBER_DOUBLE, float(m.group())))
                i += len(m.group())
                continue

            m = self.NUMBER_INT_RE.match(chunk)
            if m:
                tokens.append((self.NUMBER_INT, int(m.group())))
                i += len(m.group())
                continue

            m = self.INDENT_START_RE.match(chunk)
            if m:
                indent = len(m.group(1))
                if indent <= indents[-1]:
                    raise LexerError('expected an indented block')
                indents.append(indent)
                tokens.append((self.INDENT, indent))
                i += indent + len(':\n')
                continue

            m = self.INDENT_CONT_RE.match(chunk)
            if m:
                indent = len(m.group(1))
                if indent == indents[-1]:
                    tokens.append(('\n', '\n'))
                elif indent > indents[-1]:
                    raise LexerError('unexpected indent')
                else:
                    if indent not in indents:
                        raise LexerError('unindent does not match any outer indentation level')
                    while indent < indents[-1]:
                        indents.pop()
                        tokens.append((self.DEDENT, indents[-1]))
                    tokens.append(('\n', '\n'))
                i += indent + len('\n')
                continue

            m = self.OPERATOR_RE.match(chunk)
            if m:
                op = m.group()
                tokens.append((op, op))
                i += len(op)
                continue

            if chunk.startswith(' '):
                i += 1
                continue

            raise LexerError('bad {!r}'.format(chunk))

        while indents[-1] != 0:
            indents.pop()
            tokens.append((self.DEDENT, indents[-1]))

        return tokens


class Node:
    def __init__(self, kind, **kwargs):
        self.kind = kind
        for k, v in kwargs.items():
            setattr(self, k, v)
        # print(vars(self))

    def __repr__(self):
        return '\nNode: {!r}'.format(vars(self))

    def __eq__(self, other):
        return vars(self) == vars(other)


class ParserError(Exception):
    pass


class Parser:

    def __init__(self, tokens):
        self.tokens = tokens + [(None, None)]
        self.i = 0

    def run(self):
        node = Node('PROGRAM')
        node.body = []
        while self.i < len(self.tokens) - 1:
            node.body.append(self.statement())
        assert self.i == len(self.tokens) - 1
        return node

    def token(self):
        token = self.tokens[self.i]
        self.i += 1
        return token

    # grammar

    def term(self):
        token = self.token()

        if token[0] in (Lexer.NUMBER_INT, Lexer.NUMBER_DOUBLE):
            return Node('CONST', value=token[1])

        if token[0] == Lexer.IDENT:
            name = token[1]
            token = self.token()
            if token[0] == '(':
                n = Node('CALL', func=name)
                n.args = args = []
                token = self.token()
                self.i -= 1
                while token[0] != ')':
                    args.append(self.expression())
                    token = self.tokens[self.i]
                    if token[0] == ',':
                        token = self.token()
                self.token()
                return n
            elif token[0] == '[':
                n = Node('SUBSCRIPT', value=name)
                n.index = self.expression()
                assert self.tokens[self.i][0] == ']'
                self.token()
                return n
            else:
                self.i -= 1
                return Node('IDENT', idx=name)

        if token[0] == '(':
            n = Node('EXPR', value=self.expression())
            assert self.tokens[self.i][0] == ')'
            self.token()
            return n

        if token[0] == '[':
            n = Node('LIST')
            n.elements = elements = []
            token = self.token()
            self.i -= 1
            while token[0] != ']':
                elements.append(self.expression())
                token = self.tokens[self.i]
                if token[0] == ',':
                    token = self.token()
            self.token()
            return n

        return '-term'

    def mul(self):
        left = self.term()
        token = self.tokens[self.i]
        if token[0] not in ('*', '/', '//'):
            return left
        self.token()
        return Node(token[0], left=left, right=self.expression())

    def sum(self):
        left = self.mul()
        token = self.tokens[self.i]
        if token[0] not in ('+', '-'):
            return left
        self.token()
        return Node(token[0], left=left, right=self.expression())

    def expression(self):
        return self.sum()

    def test(self):
        node = Node('COMPARISION')
        node.left = self.expression()
        node.op = Node(self.token()[1])
        node.right = self.expression()
        return node

    def parameters(self):
        if not self.token()[0] == '(':
            raise ParserError('(')
        params = []
        token = self.token()
        while token[0] != ')':
            node = Node('IDENT', idx=token[1])  # assert IDENT
            params.append(node)
            token = self.token()
            if token[0] == ',':  # assert , or ) or IDENT
                token = self.token()
        return params

    def suite(self):
        if self.token()[0] != Lexer.INDENT:
            raise ParserError('Indent suite is expected!')
        body = []
        token = self.token()
        while token[0] != Lexer.DEDENT:
            self.i -= 1  # we already started processing
            node = self.statement()
            body.append(node)
            token = self.token()
            if token[0] == '\n':
                token = self.token()
        return body

    def statement(self):
        token = self.token()

        while token[0] == '\n':
            token = self.token()

        if token[0] == 'IF':
            node = Node(token[0])
            node.test = self.test()
            node.body = self.suite()
            return node

        if token[0] == 'WHILE':
            node = Node(token[0])
            node.test = self.test()
            node.body = self.suite()
            return node

        if token[0] == 'DEF':
            node = Node(token[0])
            node.name = self.token()[1]  # check ident
            node.parameters = self.parameters()
            node.body = self.suite()
            return node

        if token[0] == 'RETURN':
            n = Node(token[0])
            token = self.tokens[self.i]
            if token[0] not in ('\n', 'DEDENT'):
                n.value = self.expression()
            else:
                n.value = Node('CONST', value=0)
            return n

        self.i -= 1
        node = Node('EXPR', value=self.expression())
        if node.value.kind in ('IDENT', 'SUBSCRIPT') and self.tokens[self.i][0] == '=':
            self.token()
            return Node('ASSIGN', target=node.value, value=self.expression())
        return node


class Compiler:
    def __init__(self):
        self.code = []
        self.consts = []
        self.names = []
        self.varnames = []

    def gen(self, command, const=None, name=None, varname=None):
        self.code.append(command)
        if const is not None:
            if const in self.consts:
                self.code.append(self.consts.index(const))
            else:
                self.code.append(len(self.consts))
                self.consts.append(const)
        if name is not None:
            if name in self.names:
                self.code.append(self.names.index(name))
            else:
                self.code.append(len(self.names))
                self.names.append(name)
        if varname is not None:
            self.code.append(self.varnames.index(varname))

    def run(self, node):
        # TODO: LOAD_NAME/FAST

        if node.kind == 'PROGRAM':
            for n in node.body:
                self.run(n)
            # exit
            self.gen('LOAD_CONST', const=0)
            self.gen('RETURN_VALUE')

        elif node.kind == 'ASSIGN':
            self.run(node.value)
            if node.target.kind == 'IDENT':
                idx = node.target.idx
                if idx in self.varnames:
                    self.gen('STORE_FAST', varname=idx)
                else:
                    self.gen('STORE_NAME', name=idx)
            elif node.target.kind == 'SUBSCRIPT':
                self.gen('LOAD_NAME', name=node.target.value)
                self.run(node.target.index)
                self.gen('STORE_SUBSCR')
            else:
                raise RuntimeError('Cannot assign!')

        elif node.kind == 'CONST':
            self.gen('LOAD_CONST', const=node.value)

        elif node.kind == 'LIST':
            for n in node.elements:
                self.run(n)
            self.gen('BUILD_LIST')
            self.gen(len(node.elements))

        elif node.kind in ('+', '-', '*', '/', '//'):
            self.run(node.left)
            self.run(node.right)
            self.gen('BINARY_' + node.kind)

        elif node.kind == 'IDENT':
            idx = node.idx
            if idx in self.varnames:  # TODO!
                self.gen('LOAD_FAST', varname=idx)
            else:
                self.gen('LOAD_NAME', name=idx)

        elif node.kind == 'DEF':
            func_compiler = Compiler()
            func_compiler.varnames[:] = [n.idx for n in node.parameters]  # TODO!
            func_compiler.run(Node('PROGRAM', body=node.body))
            self.gen('LOAD_CONST', const=func_compiler)
            self.gen('LOAD_CONST', const=node.name)
            self.gen('MAKE_FUNCTION')
            self.gen(0)  # pretend python
            self.gen('STORE_NAME', name=node.name)

        elif node.kind == 'IF':
            self.run(node.test)
            self.gen('POP_JUMP_IF_FALSE')
            addr = len(self.code)
            self.gen('FAKE_ADDR')  # will be set after IF
            for n in node.body:
                self.run(n)
            self.code[addr] = len(self.code)

        elif node.kind == 'COMPARISION':
            self.run(node.left)
            self.run(node.right)
            self.gen('COMPARE_OP')
            self.gen(node.op.kind)

        elif node.kind == 'EXPR':
            self.run(node.value)

        elif node.kind == 'CALL':
            self.gen('LOAD_NAME', name=node.func)
            for n in node.args:
                self.run(n)
            self.gen('CALL_FUNCTION')
            self.gen(len(node.args))

        elif node.kind == 'RETURN':
            self.run(node.value)
            self.gen('RETURN_VALUE')

        elif node.kind == 'SUBSCRIPT':
            self.gen('LOAD_NAME', name=node.value)
            self.run(node.index)
            self.gen('BINARY_SUBSCR')

        elif node.kind == 'WHILE':
            addr_while = len(self.code)
            self.run(node.test)
            self.gen('POP_JUMP_IF_FALSE')
            addr = len(self.code)
            self.gen('FAKE_ADDR')  # will be set after WHILE
            for n in node.body:
                self.run(n)
            self.gen('JUMP_ABSOLUTE')
            self.gen(addr_while)
            self.code[addr] = len(self.code)

        else:
            raise RuntimeError('UNEXPECTED NODE!')


class PrintFunction:
    def run(self, args):
        print(*args)
        return 0


class LenFunction:
    def run(self, args):
        return len(args[0])


class Frame:
    def __init__(self, code_obj, global_names, local_names, prev_frame):
        self.code_obj = code_obj
        self.global_names = global_names
        self.local_names = local_names
        self.prev_frame = prev_frame

        self.builtin_names = {
            'print': PrintFunction(),
            'len': LenFunction(),
        }
        self.stack = []
        self.last_instruction = 0


class Function:
    def __init__(self, name, code_obj, global_names, vm):
        self._vm = vm
        self.name = name
        self.code_obj = code_obj
        self.global_names = global_names

        self.locals_names = vm.frame.local_names

    def run(self, args):
        callargs = dict(zip(self.code_obj.varnames, args))

        frame = self._vm.make_frame(self.code_obj, callargs, self.global_names, {})
        return self._vm.run_frame(frame)


class VirtualMachine:
    def __init__(self):
        self.frames = []
        self.return_value = None

    @property
    def frame(self):
        if self.frames:
            return self.frames[-1]
        return None

    def run(self, code_obj):
        frame = self.make_frame(code_obj)
        self.run_frame(frame)

    def make_frame(self, code_obj, args=None, global_names=None, local_names=None):
        if global_names is not None:
            if local_names is None:
                local_names = global_names
        elif self.frame:
            global_names = self.frame.global_names
            local_names = {}
        else:
            global_names = local_names = {}
        local_names.update(args or {})
        frame = Frame(code_obj, global_names, local_names, self.frame)
        return frame

    def push_frame(self, frame):
        self.frames.append(frame)

    def pop_frame(self):
        self.frames.pop()

    def run_frame(self, frame):
        self.push_frame(frame)
        while self.run_code() is None:
            pass
        self.pop_frame()
        return self.return_value

    def top(self):
        return self.frame.stack[-1]

    def pop(self):
        return self.frame.stack.pop()

    def push(self, *vals):
        self.frame.stack.extend(vals)

    def popn(self, n):
        if n:
            ret = self.frame.stack[-n:]
            self.frame.stack[-n:] = []
            return ret
        else:
            return []

    def parse_byte_and_arg(self):
        f = self.frame
        byte = f.code_obj.code[f.last_instruction]
        f.last_instruction += 1
        if byte in ('RETURN_VALUE', 'STORE_SUBSCR'):
            arg = None
        elif byte.startswith('BINARY_'):
            arg = None
        else:
            arg = f.code_obj.code[f.last_instruction]
            f.last_instruction += 1
            if byte in ('LOAD_CONST',):
                arg = f.code_obj.consts[arg]
            elif byte in ('LOAD_NAME', 'STORE_NAME'):
                arg = f.code_obj.names[arg]
            elif byte in ('LOAD_FAST', 'STORE_FAST'):
                arg = f.code_obj.varnames[arg]
        return byte, arg

    def dispatch(self, byte, arg):
        if byte == 'RETURN_VALUE':
            self.return_value = self.pop()
            return 'return'

        elif byte == 'LOAD_CONST':
            self.push(arg)

        elif byte == 'LOAD_FAST':
            f = self.frame
            if arg in f.local_names:
                val = f.local_names[arg]
            else:
                raise RuntimeError('LOCAL NAME %s HAS NO VALUE YET' % arg)
            self.push(val)

        elif byte == 'LOAD_NAME':
            f = self.frame
            if arg in f.local_names:
                val = f.local_names[arg]
            elif arg in f.global_names:
                val = f.global_names[arg]
            elif arg in f.builtin_names:
                val = f.builtin_names[arg]
            else:
                raise RuntimeError('NOT DEFINED NAME %s' % arg)
            self.push(val)

        elif byte == 'STORE_FAST':
            self.frame.local_names[arg] = self.pop()

        elif byte == 'STORE_NAME':
            self.frame.local_names[arg] = self.pop()

        elif byte == 'STORE_SUBSCR':
            val, a, i = self.popn(3)
            a[i] = val

        elif byte == 'BUILD_LIST':
            elements = self.popn(arg)
            self.push(elements)

        elif byte == 'BINARY_SUBSCR':
            a, i = self.popn(2)
            self.push(a[i])

        elif byte.startswith('BINARY_'):
            x, y = self.popn(2)
            ops = {
                '+': operator.add,
                '-': operator.sub,
                '*': operator.mul,
                '/': operator.truediv,
                '//': operator.floordiv,
            }
            self.push(ops[byte[len('BINARY_'):]](x, y))

        elif byte == 'COMPARE_OP':
            x, y = self.popn(2)
            ops = {
                '<': operator.lt,
                '>': operator.gt,
                '<=': operator.le,
                '=>': operator.ge,
                '==': operator.eq,
            }
            self.push(ops[arg](x, y))

        elif byte == 'POP_JUMP_IF_FALSE':
            val = self.pop()
            if not val:
                self.frame.last_instruction = arg

        elif byte == 'JUMP_ABSOLUTE':
            self.frame.last_instruction = arg

        elif byte == 'MAKE_FUNCTION':
            name = self.pop()
            code_obj = self.pop()
            func = Function(name, code_obj, dict(self.frame.global_names), self)
            self.push(func)

        elif byte == 'CALL_FUNCTION':
            args = self.popn(arg)
            func = self.pop()
            retval = func.run(args)
            self.push(retval)

        else:
            raise RuntimeError('UNEXPECTED CODE!')

    def run_code(self):
        byte, arg = self.parse_byte_and_arg()
        return self.dispatch(byte, arg)


# lexer = Lexer()
# parser = Parser(lexer)
# compiler = Compiler(parser)
# VirtualMachine(compiler)
