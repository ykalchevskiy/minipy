import dis
import pprint

from main import Compiler, Parser, Lexer

"""
def bs(a, x):
    l = 0
    r = len(a) - 1
    while l <= r:
        m = (l + r) // 2
        v = a[m]
        if v == x:
            return m
        if v > x:
            r = m - 1
        if v < x:
            l = m + 1
    return -1


dis.dis(bs)

print()
print()
print()

# dis.dis(""
def bs(a, x):
    l = 0
    r = len(a) - 1
    while l <= r:
        m = (l + r) // 2
        v = a[m] 
        if v == x:
            return m 
        if v > x:
            r = m - 1
        if v < x:
            l = m + 1
    return -1

a = [1, 2]
print(binary_search(a, 1))
x = 0
print(binary_search(a, x))
# """

example = """\
def binary_search(a, x):
    l = 0
    r = len(a) - 1
    while l <= r:
        m = (l + r) // 2
        v = a[m] 
        if v == x:
            return m 
        if v > x:
            r = m - 1
        if v < x:
            l = m + 1
    return -1

a = [1, 2]
print(binary_search(a, 1))
x = 0
print(binary_search(a, x))
"""
example = """\
def f():
    return
f()
"""
lexer = Lexer()
tokens = lexer.run(example)
print(tokens)
parser = Parser(tokens)
ast = parser.run()
print(ast)  #
compiler = Compiler()
compiler.run(ast)
print()
dis.dis(example)  #
print()
print(compiler)
print(compiler.code)
print(compiler.consts)
print(compiler.names)
# print()
# print(compiler.consts[0].code)
# print(compiler.consts[0].consts)
# print(compiler.consts[0].names)
