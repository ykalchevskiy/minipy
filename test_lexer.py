import textwrap
from unittest import TestCase


from main import Lexer, LexerError


class TestLexer(TestCase):
    def assert_lexer(self, code, tokens):
        self.assertEqual(Lexer().run(code), tokens)

    def assert_lexer_error(self, code, message):
        with self.assertRaises(LexerError) as e:
            Lexer().run(code)
        self.assertEqual(str(e.exception), message)

    def test_int(self):
        self.assert_lexer('0', [(Lexer.NUMBER_INT, 0)])
        self.assert_lexer('5', [(Lexer.NUMBER_INT, 5)])
        self.assert_lexer('10', [(Lexer.NUMBER_INT, 10)])
        self.assert_lexer('256', [(Lexer.NUMBER_INT, 256)])
        self.assert_lexer('-5', [(Lexer.NUMBER_INT, -5)])
        self.assert_lexer('-10', [(Lexer.NUMBER_INT, -10)])
        self.assert_lexer('-256', [(Lexer.NUMBER_INT, -256)])

    def test_double(self):
        self.assert_lexer('0.0', [(Lexer.NUMBER_DOUBLE, 0.0)])
        self.assert_lexer('0.', [(Lexer.NUMBER_DOUBLE, 0.0)])
        self.assert_lexer('.0', [(Lexer.NUMBER_DOUBLE, 0.0)])
        self.assert_lexer('1456.6', [(Lexer.NUMBER_DOUBLE, 1456.6)])
        self.assert_lexer('01456.6', [(Lexer.NUMBER_DOUBLE, 1456.6)])
        self.assert_lexer('-1456.6', [(Lexer.NUMBER_DOUBLE, -1456.6)])
        self.assert_lexer('-01456.6', [(Lexer.NUMBER_DOUBLE, -1456.6)])

    def test_ident(self):
        self.assert_lexer('a', [(Lexer.IDENT, 'a')])
        self.assert_lexer('a13_123', [(Lexer.IDENT, 'a13_123')])
        self.assert_lexer('ASDASD', [(Lexer.IDENT, 'ASDASD')])

    def test_keywords(self):
        self.assert_lexer('if', [('IF', 'if')])
        self.assert_lexer('while 1', [('WHILE', 'while'), (Lexer.NUMBER_INT, 1)])
        self.assert_lexer('def bs', [('DEF', 'def'), (Lexer.IDENT, 'bs')])

    def test_indent(self):
        self.assert_lexer('a\na', [(Lexer.IDENT, 'a'), ('\n', '\n'), (Lexer.IDENT, 'a')])

        self.assert_lexer('if x:\n x', [
            ('IF', 'if'), (Lexer.IDENT, 'x'),
            (Lexer.INDENT, 1),
             (Lexer.IDENT, 'x'),
            (Lexer.DEDENT, 0),
        ])

        self.assert_lexer('if x:\n x\n x', [
            ('IF', 'if'), (Lexer.IDENT, 'x'),
            (Lexer.INDENT, 1),
             (Lexer.IDENT, 'x'), ('\n', '\n'),
             (Lexer.IDENT, 'x'),
            (Lexer.DEDENT, 0),
        ])

        self.assert_lexer(textwrap.dedent("""\
            if 1:
                2
                3
                if 4:
                    5
                else:
                    6
                    7
                8
            9
            """), [
            ('IF', 'if'), (Lexer.NUMBER_INT, 1),
            (Lexer.INDENT, 4),
                (Lexer.NUMBER_INT, 2), ('\n', '\n'),
                (Lexer.NUMBER_INT, 3), ('\n', '\n'),
                ('IF', 'if'), (Lexer.NUMBER_INT, 4),
                (Lexer.INDENT, 8),
                    (Lexer.NUMBER_INT, 5),
                (Lexer.DEDENT, 4), ('\n', '\n'),
                ('ELSE', 'else'),
                (Lexer.INDENT, 8),
                    (Lexer.NUMBER_INT, 6), ('\n', '\n'),
                    (Lexer.NUMBER_INT, 7),
                (Lexer.DEDENT, 4), ('\n', '\n'),
                (Lexer.NUMBER_INT, 8),
            (Lexer.DEDENT, 0), ('\n', '\n'),
            (Lexer.NUMBER_INT, 9),
        ])

        self.assert_lexer(
            textwrap.dedent("""\
            if 1:
                if 1:
                    if 1:
                        1
            """), [
                ('IF', 'if'), (Lexer.NUMBER_INT, 1),
                (Lexer.INDENT, 4),
                    ('IF', 'if'), (Lexer.NUMBER_INT, 1),
                    (Lexer.INDENT, 8),
                        ('IF', 'if'), (Lexer.NUMBER_INT, 1),
                        (Lexer.INDENT, 12),
                            (Lexer.NUMBER_INT, 1),
                        (Lexer.DEDENT, 8),
                    (Lexer.DEDENT, 4),
                (Lexer.DEDENT, 0),
            ])

    def test_indent_errors(self):
        self.assert_lexer_error(' 1', 'unexpected indent')

        self.assert_lexer_error(
            textwrap.dedent("""\
            if 1:
                if 1:
                1
            """),
            'expected an indented block'
        )

        self.assert_lexer_error(
            textwrap.dedent("""\
            if 1:
                1
                    2
            """),
            'unexpected indent'
        )

        self.assert_lexer_error(
            textwrap.dedent("""\
            if 1:
                1
               1
            """),
            'unindent does not match any outer indentation level'
        )

    def test_binary_search(self):
        self.maxDiff = None
        self.assert_lexer(textwrap.dedent("""\
            def binary_search(array, x):
                l = 0
                r = len(array) - 1
                while l <= r:
                    m = (l + r) // 2
                    if array[m] == x:
                        return m
                    if array[m] > x:
                        r = m - 1
                    else:
                        l = m + 1
                return -1
            
            a = [1, 2]
            print(binary_search(a, 1))
            x = 0
            print(binary_search(a, x))
            
            """), [
            ('DEF', 'def'), (Lexer.IDENT, 'binary_search'), ('(', '('), (Lexer.IDENT, 'array'), (',', ','), (Lexer.IDENT, 'x'), (')', ')'),
            (Lexer.INDENT, 4),
                (Lexer.IDENT, 'l'), ('=', '='), (Lexer.NUMBER_INT, 0), ('\n', '\n'),
                (Lexer.IDENT, 'r'), ('=', '='), (Lexer.IDENT, 'len'), ('(', '('), (Lexer.IDENT, 'array'), (')', ')'), ('-', '-'), (Lexer.NUMBER_INT, 1), ('\n', '\n'),
                ('WHILE', 'while'), (Lexer.IDENT, 'l'), ('<=', '<='), (Lexer.IDENT, 'r'),
                (Lexer.INDENT, 8),
                    (Lexer.IDENT, 'm'), ('=', '='), ('(', '('), (Lexer.IDENT, 'l'), ('+', '+'), (Lexer.IDENT, 'r'),  (')', ')'), ('//', '//'), (Lexer.NUMBER_INT, 2), ('\n', '\n'),
                    ('IF', 'if'), (Lexer.IDENT, 'array'), ('[', '['), (Lexer.IDENT, 'm'), (']', ']'), ('==', '=='), (Lexer.IDENT, 'x'),
                    (Lexer.INDENT, 12),
                        ('RETURN', 'return'), (Lexer.IDENT, 'm'),
                    (Lexer.DEDENT, 8), ('\n', '\n'),
                    ('IF', 'if'), (Lexer.IDENT, 'array'), ('[', '['), (Lexer.IDENT, 'm'), (']', ']'), ('>', '>'), (Lexer.IDENT, 'x'),
                    (Lexer.INDENT, 12),
                        (Lexer.IDENT, 'r'), ('=', '='), (Lexer.IDENT, 'm'), ('-', '-'), (Lexer.NUMBER_INT, 1),
                    (Lexer.DEDENT, 8), ('\n', '\n'),
                    ('ELSE', 'else'),
                    (Lexer.INDENT, 12),
                        (Lexer.IDENT, 'l'), ('=', '='), (Lexer.IDENT, 'm'), ('+', '+'), (Lexer.NUMBER_INT, 1),
                    (Lexer.DEDENT, 8),
                (Lexer.DEDENT, 4), ('\n', '\n'),
                ('RETURN', 'return'), (Lexer.NUMBER_INT, -1),
            (Lexer.DEDENT, 0), ('\n', '\n'),
            ('\n', '\n'),
            (Lexer.IDENT, 'a'), ('=', '='), ('[', '['), (Lexer.NUMBER_INT, 1), (',', ','), (Lexer.NUMBER_INT, 2), (']', ']'), ('\n', '\n'),
            (Lexer.IDENT, 'print'), ('(', '('), (Lexer.IDENT, 'binary_search'), ('(', '('), (Lexer.IDENT, 'a'), (',', ','), (Lexer.NUMBER_INT, 1), (')', ')'), (')', ')'), ('\n', '\n'),
            (Lexer.IDENT, 'x'), ('=', '='), (Lexer.NUMBER_INT, 0), ('\n', '\n'),
            (Lexer.IDENT, 'print'), ('(', '('), (Lexer.IDENT, 'binary_search'), ('(', '('), (Lexer.IDENT, 'a'), (',', ','), (Lexer.IDENT, 'x'), (')', ')'), (')', ')')
        ])
