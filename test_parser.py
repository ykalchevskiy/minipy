from main import Lexer, Parser

lexer = Lexer()
tokens = lexer.run('if 1 > 1:\n 1')
tokens2 = lexer.run('a[1] + 1')
tokens2 = lexer.run("""\
def bs(a, x):
    l = 0
    r = len(a) - 1
    while l <= r:
        m = (l + r) // 2
        v = a[m] 
        if v == x:
            return m 
        if v > x:
            r = m - 1
        if v < x:
            l = m + 1
    return -1

a = [1, 2]
print(binary_search(a, 1))
x = 0
print(binary_search(a, x))
""")
print(tokens)
print(Parser(tokens).run())
