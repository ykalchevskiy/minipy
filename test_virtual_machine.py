from main import VirtualMachine, Compiler, Parser, Lexer


example = """\
def binary_search(a, x):
    l = 0
    r = len(a) - 1
    while l <= r:
        m = (l + r) // 2
        v = a[m]
        if v == x:
            return m 
        if v > x:
            r = m - 1
        if v < x:
            l = m + 1
    return -1

b = [1.1, 2.1, 3.1]
b[1 - 2] = 4.1
y = 0.1
while y < 5:
 print(y, binary_search(b, y))
 y = y + 1
a = [[1]]
print(a[0])
"""

example2 = """\
def f(n):
 if n == 1:
  return 1
 return f(n - 1) * n

a = f(5)
print(a)
"""

example = """\
def f():
    if 3 > 2:
     return
    print(1)
f()
"""

example2 = """\
def bs(a, x, l, r):
  if l > r:
   return -1
  m = (l + r) // 2
  v = a[m]
  if v == x:
   return m
  if v > x:
   return bs(a, x, l, m - 1)
  return bs(a, x, m + 1, r)

b = [1, 2, 3]
print(bs(b, 3, 0, len(b) - 1))
"""

lexer = Lexer()
tokens = lexer.run(example)
parser = Parser(tokens)
ast = parser.run()
compiler = Compiler()
compiler.run(ast)
vm = VirtualMachine()
vm.run(compiler)
